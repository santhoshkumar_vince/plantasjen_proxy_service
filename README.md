# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A aws-cdk nodejs backend service for creating APIs to connect to plantasjen proxy APIs.
* version: 1.0.0

### How do I get set up? ###

* created aws-cdk project with aws-cdk CLI
* created lambdas to get connection with plantasjen proxy, by making API calls.
* 'axios' module is used to make API call to proxy and get response back.
* to make build before deployment: $npm run build
* to deploy into aws: cdk deploy

### Who do I talk to? ###

* Vince AS team